package com.calculadora.ws;

import javax.jws.WebService;

/**
 * Clase encargada de realizar las operaciones matematicas de acuerdo a la oción enviada
 * desde el cliente
 */
@WebService(endpointInterface = "com.calculadora.ws.Calculadora")
public class CalculadoraImpl implements Calculadora {

    @Override
    public double operacion(int opcion, double valorUno, double valorDos) {

        double resultado = 0.0d;

        switch (opcion) {
            case 1:
                resultado = valorUno + valorDos;
                break;
            case 2:
                resultado = valorUno - valorDos;
                break;
            case 3:
                resultado = valorUno * valorDos;
                break;
            case 4:
                resultado = valorUno / valorDos;
                break;
            default:
                break;
        }
        return resultado;
    }
}
